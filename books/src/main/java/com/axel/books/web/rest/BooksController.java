package com.axel.books.web.rest;

import com.axel.books.dto.BooksDto;
import com.axel.books.model.Books;
import com.axel.books.service.BooksService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BooksController {

    @Autowired
    BooksService booksService;

    @Autowired
    private HttpServletRequest request;

    @GetMapping("")
    public List<BooksDto> find() {

        Iterator<Books> iterator = booksService.getAll().iterator();

        List<BooksDto> booksDtoList = new ArrayList<>();
        while (iterator.hasNext()) {
            Books books = iterator.next();
            booksDtoList.add(toDto(books));
        }

        return booksDtoList;
    }
    @PostMapping("")
    public BooksDto save(@RequestParam("image") MultipartFile file, @RequestParam("judul") String judul,
                       @RequestParam("harga") BigDecimal harga, @RequestParam("pengarang") String pengarang,
                       @RequestParam("deskripsi") String deskripsi, @RequestParam("stok") int stok) {
        String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
        if (!fileType.equalsIgnoreCase("jpeg") && !fileType.equalsIgnoreCase("png") && !fileType.equalsIgnoreCase("jpg"))  {
            throw new RuntimeException("File must be image");
        }

        Books books = new Books();
        books.setJudul(judul);
        books.setDeskripsi(deskripsi);
        books.setHarga(harga);
        books.setPengarang(pengarang);
        books.setStok(stok);


        try {
            String uploadsDir = "/uploads/";
            String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
            if(! new File(realPathtoUploads).exists())
            {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = file.getOriginalFilename();
            String filePath = realPathtoUploads + orgName;
            File dest = new File(filePath);
            file.transferTo(dest);
            books.setImage(filePath);
        }catch (IOException e) {
            e.printStackTrace();
        }

        books = booksService.save(books);

        return toDto(books);
    }


    @PatchMapping("")
    public BooksDto update( @RequestBody BooksDto booksDto) {

        Books books = booksService.findById(booksDto.getId());
        books.setStok(booksDto.getStok());

        books = booksService.save(books);

        return toDto(books);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable String id){
        Books books = booksService.findById(id);
        booksService.delete(books);
    }

    @DeleteMapping("/all")
    public void deleteAll(){
        booksService.deleteAll();
    }

    private BooksDto toDto(Books books){
        BooksDto booksDto = new BooksDto();
        booksDto.setDeskripsi(books.getDeskripsi());
        booksDto.setHarga(books.getHarga());
        booksDto.setId(books.getId());
        booksDto.setImage(books.getImage());
        booksDto.setJudul(books.getJudul());
        booksDto.setPengarang(books.getPengarang());
        booksDto.setStok(books.getStok());

        return booksDto;
    }

    private Books fromDto(BooksDto booksDto){
        Books books = new Books();

        books.setJudul(booksDto.getJudul());
        books.setDeskripsi(booksDto.getDeskripsi());
        books.setHarga(booksDto.getHarga());
        books.setPengarang(booksDto.getPengarang());
        books.setImage(booksDto.getImage());
        books.setStok(booksDto.getStok());

        return books;
    }

}
