package com.axel.books.repository;

import com.axel.books.model.Books;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends PagingAndSortingRepository<Books, String> {
}
