package com.axel.books.service.impl;

import com.axel.books.model.Books;
import com.axel.books.repository.BooksRepository;
import com.axel.books.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class BooksServiceImpl implements BooksService {

    @Autowired
    private BooksRepository booksRepository;

    @Override
    public Books save(Books books) {
        return booksRepository.save(books);
    }

    @Override
    public Books findById(String id) {

        Books books;

        Optional<Books> booksOptional = booksRepository.findById(id);

        if (booksOptional.isPresent())
        {
            books = booksOptional.get();
        }
        else
        {
            throw new RuntimeException("Books not Found");
        }

        return books;

    }

    @Override
    public Iterable<Books> getAll() {
        return booksRepository.findAll();
    }

    @Override
    public void delete(Books books) {
        booksRepository.delete(books);
    }

    @Override
    public void deleteAll() {
        booksRepository.deleteAll();
    }


}
