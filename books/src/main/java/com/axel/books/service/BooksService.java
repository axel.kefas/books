package com.axel.books.service;

import com.axel.books.model.Books;

public interface BooksService {

    Books save(Books books);

    Books findById(String id);

    void delete(Books books);

    Iterable<Books> getAll();

    void deleteAll();
}
