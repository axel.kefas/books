package com.axel.books.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BooksDto {

    private String id;
    private String judul;
    private String pengarang;
    private String deskripsi;
    private String image;
    private BigDecimal harga;
    private int stok;
}
